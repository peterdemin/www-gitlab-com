---
layout: handbook-page-toc
title: Engagement Concierge
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# GitLab Team Engagement Concierge

## Program Overview

The Engagement Concierge sits within the Talent & Engagement team and is a service that Director+ team leaders can utilize to create bespoke and customized engagement programs, delivered primarily through learning activities.

Reasons to collaborate with the Engagement Concierge can range from team building needs through to specific learning needs. As a leader, you may want to become more intentional around your team dynamics, team member sense of trust & belonging in an all-remote environment, or specific group learning needs (e.g. [Crucial Conversations](/handbook/leadership/crucial-conversations/)). Once approved for the program, a member of the Talent & Engagement team will collaborate with you to find the right initiative or solution for your team. 

## Features

Visit each page below to learn about existing Engagement Concierge programs!

1. [Commerical Sales - Boundary Breakers](/handbook/people-group/engagement-concierge/boundary-breakers)

## Expectations

The Talent & Engagement team will advise and consult on appropriate activities and help build bespoke solutions for your team, but will only manage limited logistics. For example, if the initiative is a planned series of Learning & Development activities, we will build the learning path in Level Up, define the learning schedule, and if you choose to engage any external facilitation, introduce you to our preferred vendors. However, the requester will be responsible for PO creation, communicating requirements to your team, calendaring, and any other logistical requirements.  

There may be situations where the Talent & Engagement team identifies that the team challenge could benefit from input from other teams within the People Group. We will refer you to them as appropriate.

## Requesting Engagement Concierge Services

Due to current capacity constraints, the Talent & Engagement team will triage requests as received, communicate next steps, and share a proposed timeline with you as quickly as possible. Our current capacity allows for 2-3 bespoke engagement projects per quarter, depending on project size and complexity.

To request support from the Engagement Concierge, please open an issue using the [`engagement-concierge` issue template](https://gitlab.com/gitlab-com/people-group/learning-development/custom-ld-engagement/-/issues/new#). When requesting support in the issue template, please tag your relevant People Business Partner (PBP) for visibility. You can find the PBP that supports your division [here](/handbook/people-group/#people-business-partner-alignment-to-division).