---
layout: handbook-page-toc
title: "CSM/CSE Webinar Calendar"
---
# On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---

# Upcoming Webinars

We’d like to invite you to our free upcoming webinars in the month of August.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

## August 2023

### AMER Time Zone Webinars

#### Hands-On GitLab DevSecOps Workshop
##### August 30th, 2023 at 9:00AM-11:00PM Pacific Time / 12:00-2:00PM Eastern Time

In this workshop we will focus on how you can secure your application with GitLab. We will first take a look at how to apply scanners to your CICD pipelines in a hands-on exercise so that any vulnerabilities are caught as soon as the code is committed. Next, we will look at compliance frameworks and pipelines to show how you can ensure no one within your development teams is cutting corners and exposing your application.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_zKJjC90zQUmcCOHkYPxQaQ)

#### Hands-On GitLab DevSecOps Workshop
##### August 31st, 2023 at 3:00-4:00PM Pacific Time / 6:00-7:00PM Eastern Time / 8:00-9:00AM UTC+10

In this workshop we will focus on how you can secure your application with GitLab. We will first take a look at how to apply scanners to your CICD pipelines in a hands-on exercise so that any vulnerabilities are caught as soon as the code is committed. Next, we will look at compliance frameworks and pipelines to show how you can ensure no one within your development teams is cutting corners and exposing your application.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_uYuCYvuhTV-raOnUaPStlg)

## September 2023

### AMER Time Zone Webinars

#### Hands-On GitLab CI Workshop 
##### September 6th, 2023 at 9:00-10:30AM Pacific Time / 12:00-1:30PM Eastern Time

Join us for a hands-on GitLab CI workshop and learn how it can fit in your organization!

Learn how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_KewiOoVOQ5-XnYQ5_hdk1g#)

#### Git Basics
##### September 8th, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

New to Git? Join this webinar targeted at beginners working with source code, where we will review the basics of using Git for version control and how it works with GitLab to help you get started quickly.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_yizF5bvKRmWMzfTNCl-FIA#/registration)

#### Intro to GitLab
##### September 12th, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_t6S-sdvsQn-S5efqQl5cXg#/registration)

#### Hands-On GitLab CI Workshop for Jenkins Users
##### September 12th, 2023 at 3:30-5:00PM Pacific Time / 6:30-7:00PM Eastern Time / 8:30-9:00AM UTC+10

Join us for a hands-on GitLab CI workshop and learn how it can fit in your organization! 

We will kick things off by going over the differences between CI/CD in Jenkins and GitLab, syntax requirements, advantages to using GitLab, and how you can achieve the same outcomes in GitLab. Getting started with CI/CD in GitLab will take a lot less time than tends to be required for Jenkins, and your users can stay in a single platform. 

We will then dive into how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_bAlQkD6GT8WPNZSp_8-Nig#/registration)

#### Hands-On GitLab CI Workshop for Jenkins Users
##### September 13th, 2023 at 9:00-11:00AM Pacific Time / 12:00-2:00PM Eastern Time

Join us for a hands-on GitLab CI workshop and learn how it can fit in your organization!

We will kick things off by going over the differences between CI/CD in Jenkins and GitLab, syntax requirements, advantages to using GitLab, and how you can achieve the same outcomes in GitLab. Getting started with CI/CD in GitLab will take a lot less time than tends to be required for Jenkins, and your users can stay in a single platform. 

We will then dive into how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_S5d4hSF7SUKrbFTTzeRU0A#/registration)

#### Intro to CI/CD
##### September 15th, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_mFaQrn5YTcu1NltBcqv8lg#/registration)


#### Advanced CI/CD
##### September 19th, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_9zBEjKyNSHSLM62njAvTBw#/registration)

#### Continuous Change Management in a Secure Way
##### September 20th, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Learn from our director of engineering how to shift left your change management process and why that is important. You will learn about the why and how of a safe and secure change management process.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_KNavz0X4TGWMIm2NmIWeBA#/registration)

#### DevSecOps/Compliance
##### September 26th, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_tZP2Hn0FRESxhIWg5TFz2A#/registration)

#### Hands-On GitLab DevSecOps Workshop
##### September 27th, 2023 at 3:00-4:00PM Pacific Time / 6:00-7:00PM Eastern Time / 8:00-9:00AM UTC+10

In this workshop we will focus on how you can secure your application with GitLab. 

We will first take a look at how to apply scanners to your CICD pipelines in a hands-on exercise so that any vulnerabilities are caught as soon as the code is committed. 

Next, we will look at compliance frameworks and pipelines to show how you can ensure no one within your development teams is cutting corners and exposing your application.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_mewzOr2JQeKKXeq8RcUAXA#/registration)

#### Hands-On GitLab DevSecOps Workshop
##### September 29th, 2023 at 9:00-11:00AM Pacific Time / 12:00-2:00PM Eastern Time

In this workshop we will focus on how you can secure your application with GitLab. 

We will first take a look at how to apply scanners to your CICD pipelines in a hands-on exercise so that any vulnerabilities are caught as soon as the code is committed. 

Next, we will look at compliance frameworks and pipelines to show how you can ensure no one within your development teams is cutting corners and exposing your application.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_sBib1IoCS6-ChIsigMW7QQ#/registration)

### EMEA Time Zone Webinars

#### Git Basics
##### September 5th, 2023 at 9:00-10:00AM UTC / 11:00AM-12:00PM CEST

New to Git? Join this webinar targeted at beginners working with source code, where we will review the basics of using Git for version control and how it works with GitLab to help you get started quickly.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_fpwjqQAPRUav_BF-44CMHw#/registration)

#### Hands-On GitLab CI Workshop
##### September 6th, 2023 at 9:00-10:30AM UTC / 11:00AM-12:30PM CEST

Join us for a hands-on GitLab CI workshop and learn how it can fit in your organization!

Learn how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN__OTspNyGTo-IP9BmE2-PdQ#)

#### Intro to GitLab
##### September 8th, 2023 at 9:00-10:00AM UTC / 11:00AM-12:00PM CEST

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_qO0JbHVYR7ev3QQ6tWKUHg#/registration)

#### Intro to CI/CD
##### September 12th, 2023 at 9:00-10:00AM UTC / 11:00AM-12:00PM CEST

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_BD7-Q11cS4-dK08Md7R8aw#/registration)

#### Advanced CI/CD
##### September 15th, 2023 at 9:00-10:00AM UTC / 11:00AM-12:00PM CEST

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_-jv0wceGTAyS7OI1n5LJ4Q#/registration)

#### Continuous Change Management in a Secure Way
##### September 19th, 2023 at 9:00-10:00AM UTC / 11:00AM-12:00PM CEST

Learn from our director of engineering how to shift left your change management process and why that is important. You will learn about the why and how of a safe and secure change management process.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_Xi9aIAKzSlKECrMkCmO9Aw#/registration)

#### DevSecOps/Compliance
##### September 26th, 2023 at 9:00-10AM UTC / 11:00AM-12:00PM CEST

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_SuqeCTzvSce0xlehGJT3Tg#/registration)

#### Hands-On GitLab CI Workshop for Jenkins Users
##### September 28th, 2023 at 11:00AM-1:00PM UTC / 12:00PM-2:00PM CEST

Join us for a hands-on GitLab CI workshop and learn how it can fit in your organization! 

We will kick things off by going over the differences between CI/CD in Jenkins and GitLab, syntax requirements, advantages to using GitLab, and how you can achieve the same outcomes in GitLab. Getting started with CI/CD in GitLab will take a lot less time than tends to be required for Jenkins, and your users can stay in a single platform. 

We will then dive into how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_MfdQnhtyS1qLVLJzZBflXQ#/registration)

Check back later for more webinars! 
